import React from 'react';
import {Container, Button} from 'react-bootstrap';
import collage_1 from '../images/collage_1.jpg';
import collage_2 from '../images/collage_2.jpg';
import collage_3 from '../images/collage_3.jpg';
import collage_4 from '../images/collage_4.jpg';

function Banner({data}) {
    return (
        <Container fluid className='banner-wrapper mt-5'>
            <div className='banner-text'>
                <h1>{data.head}</h1>
                <p>{data.body}</p>
                <Button> Book an Appointment</Button>
            </div>
            <div className='rounded-box'>
                <div className='image-wrapper'>
                    <img src={collage_1} alt="img1"/>
                    <img src={collage_2} alt="img2"/>
                    <img src={collage_3} alt="img3"/>
                    <img src={collage_4} alt="img4"/>
                </div>
            </div>
        </Container>
    )
}

export default Banner
