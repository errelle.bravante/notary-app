import React from 'react'

import Brand_Logo from '../images/Brand_Logo.png';
import { Link } from 'react-router-dom';

function AppNavbar() {

    let status =  true;

    return (
        <div className='nav-wrapper'>
            <div className='brand-name'>
                <img src={Brand_Logo} alt="brand_logo" style={{height: "40px", width: "40px"}}/>
                <h4>Brand Name</h4>
            </div>
            <div className='navigation-menu'>
                <Link className="span-link" to="/">Home</Link>
                <Link className="span-link" to="/">About Us</Link>
                <Link className="span-link" to="/">Services</Link>
                {(status) ? 
                    <Link className="span-link" to="/logout">Logout</Link> 
                :
                    <>
                    <Link className="span-link" to="/login">Login</Link>
                    <Link className="span-link" to="/register">Register</Link>
                    </>
                }
                

            </div>

        </div>
    )
}

export default AppNavbar
