import React from 'react'
import facebook from '../images/facebook.png'
import youtube from '../images/youtube.png'
import instagram from '../images/instagram.png'
function BotNavBar() {
    return (
        <div className='bot-nav-wrapper mt-5'>
            <div className='bot-nav-social-buttons'>
                <a href='#'><img alt='' src={facebook}/></a>
                <a href='#'><img alt='' src={youtube}/></a>
                <a href='#'><img alt='' src={instagram}/></a>
            </div>
            
            <div className='bot-nav-link-group'>
                <div className='bot-nav-site-link'>
                    <h4>
                        Site Links
                    </h4>
                    <ul className='bot-nav-span'>
                        <li className='bot-nav-link'>About Us</li>
                        <li className='bot-nav-link'>Contact Us</li>
                        <li className='bot-nav-link'>FAQ's</li>
                    </ul>
                </div>

                <div className='bot-nav-site-link'>
                    <h4>
                        Site Links
                    </h4>
                    <ul className='bot-nav-span'>
                        <li className='bot-nav-link'>About Us</li>
                        <li className='bot-nav-link'>Contact Us</li>
                        <li className='bot-nav-link'>FAQ's</li>
                    </ul>
                </div>

                <div className='bot-nav-site-link'>
                    <h4>
                        Site Links
                    </h4>
                    <ul className='bot-nav-span'>
                        <li className='bot-nav-link'>About Us</li>
                        <li className='bot-nav-link'>Contact Us</li>
                        <li className='bot-nav-link'>FAQ's</li>
                    </ul>
                </div>
            </div>

            
            
        </div>
    )
}

export default BotNavBar
