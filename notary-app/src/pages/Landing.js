import React from 'react'
import Banner from '../components/Banner'
import { Link } from 'react-router-dom';
import {Container, Button} from 'react-bootstrap'
function Landing() {

    let data = {
        head: 'no more waiting in line',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non egestas odio. Vestibulum eget turpis felis. Proin sed metus lacinia.'
    }

    return (
        <Container fluid className='landing-container'>
            <Banner data={data}/>

            <div className='services-container'>
                <div className='services-wrapper'>
                    <h1>Services Offered</h1>
                    <ul className='card-list'>
                        <li className='card'>
                            <h2>Power of Attorney</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non egestas odio. Vestibulum eget turpis felis. Proin sed metus lacinia.</p>
                            <Link className='learn-link'>Learn More</Link>
                        </li>
                        <li className='card'>
                            <h2>Power of Attorney</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non egestas odio. Vestibulum eget turpis felis. Proin sed metus lacinia.</p>
                            <Link className='learn-link'>Learn More</Link>
                        </li>
                        <li className='card'>
                            <h2>Power of Attorney</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non egestas odio. Vestibulum eget turpis felis. Proin sed metus lacinia.</p>
                            <Link className='learn-link'>Learn More</Link>
                        </li>
                    </ul>
                    <Button className="p-4 mx-auto">Book an Appointment</Button>
                </div>
            </div>            
        </Container>
        
        
    )
}

export default Landing
