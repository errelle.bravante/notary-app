//Dependecies
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {Container} from 'react-bootstrap'

//Components and Pages
import AppNavbar from './components/AppNavbar';
import Landing from './pages/Landing';
import Error from './pages/Error';
import BotNavBar from './components/BotNavBar';

function App() {
  return (
      <Container fluid className='App px-0 mx 0 '>
        <Container fluid className='app-background'>
          <Router>
            <AppNavbar/>
              <Container>
                <Routes>
                    <Route path="/" element={<Landing/>} />
                    <Route path="/*" element={<Error/>} />
                </Routes>
              </Container>
              <BotNavBar/>
          </Router>
        </Container>
      </Container>
  );
}

export default App;
